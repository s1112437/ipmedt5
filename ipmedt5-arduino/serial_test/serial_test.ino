#include "DHT.h"             // Bibliotheek voor DHT sensoren

/*
* import adafruit unified sensor
* import DHT sensor library
*/

#define dhtPin 12            // data pin

/*
 * Stel hier in welke DHT chip je gebruikt
 */
//#define dhtType DHT11        // DHT 11
#define dhtType DHT22      // DHT 22  (AM2302), AM2321
//#define dhtType DHT21      // DHT 21 (AM2301)

DHT dht(dhtPin, dhtType);    // Initialiseer de DHT bibliotheek

float humidityVal;           // luchtvochtigheid
float tempValC;              // temperatuur in graden Celcius
float tempValF;              // temperatuur in graden Fahrenheit
float heatIndexC;            // gevoelstemperatuur in graden Celcius
float heatIndexF;            // gevoelstemperatuur in graden Fahrenheit

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

void setup() {
  Serial.begin(9600);        // stel de seriële monitor in
  dht.begin();               // start het DHT sensor uitlezen
}

void loop() {

  if (stringComplete) {

    humidityVal = dht.readHumidity();        // vraag de luchtvochtigheid aan de DHT sensor
    tempValC = dht.readTemperature();        // vraag de temperatuur in graden Celcius aan de DHT sensor
    tempValF = dht.readTemperature(true);    // vraag de temperatuur in graden Fahrenheit aan de DHT sensor
  
    // Controleer of alle waarden goed zijn uitgelezen, zo niet probeer het opnieuw
    if (isnan(humidityVal) || isnan(tempValC) || isnan(tempValF)) {
      Serial.println("Uitlezen van DHT sensor mislukt!");
  
      // Beëindig de loop() functie
      return;
    }
    
    // Bereken de gevoelstemperatuur in graden Celcius
    heatIndexC = dht.computeHeatIndex(tempValC, humidityVal, false);
  
    // Bereken de gevoelstemperatuur in graden Fahrenheit
    heatIndexF = dht.computeHeatIndex(tempValF, humidityVal);
  
  
    Serial.print("{\"temperature\":" + String(tempValC)+ ",\"humidity\":" + String(humidityVal) + ",\"heat_index\":" + String(heatIndexC) + "}");

    inputString = "";
    stringComplete = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
