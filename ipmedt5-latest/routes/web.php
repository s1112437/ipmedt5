<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {

    Route::get('/products', 'ProductController@index');

    // Route::get('/product/create', 'ProductController@createIR');
    Route::get('/product/create/ir/{errmsg}', 'ProductController@createIR');
    Route::get('/product/create/ir', 'ProductController@createIR');
    Route::post('/product/create/ir', 'ProductController@IR');
    Route::post('/product/addtocart/{product}', 'ProductController@addToCart');
    Route::post('/product/choose', 'ProductController@select')->name('choose');
    Route::get('/product/getCart','ProductController@getCart');
    Route::get('product/{id}/delete', 'ProductController@delete');
    Route::delete('product/{id}', 'ProductController@destroy');

    Route::get('/product/{id}/change', 'ProductController@change');
    Route::patch('/product/{id}/patch', 'ProductController@patch');

    Route::get('product/{id}', 'ProductController@show')->name('product');

    // Producten
    //Route::get('/products', 'ProductController@index');
    Route::get('/producten', 'ProductController@index')->name('producten');
});

// Categorien
Route::get('/category/filter', 'CategoryController@filter');
Route::get('/categories', 'CategoryController@index');

// Home
Route::get('/home', 'HomeController@index')->name('home');

// Gerechten
Route::get('/gerechten', 'GerechtController@index')->name('gerechten');
Route::get('/getIngredients', 'HomeController@getIngredients');

//Temperatuur
Route::get('/temperatuur', 'TemperatuurController@show')->name('temperatuur');

// Profile
Route::get('/profile', 'ProfileController@show')->name('show');
Route::get('/profile/settings', 'ProfileController@settings')->name('settings');
Route::put('/profile/settings', 'ProfileController@update')->name('settings');

// Houdbaarheidstore
Route::post('/storeNotif', 'HomeController@storeNotif')->name('storeNotif');
Route::post('/deleteNotif', 'HomeController@deleteNotif')->name('deleteNotif');

// Haal product plaatjes uit map buiten Laravel
Route::get('images/producten/{filename}', function ($filename) {
    $path = app_path('../../storage/producten') . '/' . $filename;

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('route.name');

// Route::fallback(function () {
//     return Redirect::to('/'); # ('/') if defined
// });
