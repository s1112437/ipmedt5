<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.head')
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    {{-- Bericht --}}
                    <span class="login100-form-title p-b-50">
                        {{ __('Wachtwoord veranderen') }}
                    </span>

                    {{-- Token --}}
                    <input type="hidden" name="token" value="{{ $token }}">

                    {{-- Email --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Onjuiste email') }}">
                        <input id="email" class="input100 @error('email') is-invalid @enderror" type="email"
                            name="email" autocomplete="email" value="{{ $email ?? old('email') }}" autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    {{-- Wachtwoord --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Voer een wachtwoord in') }}">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input id="passowrd" class="input100 @error('password') is-invalid @enderror" type="password"
                            name="password" required autocomplete="new-password">
                        <span class="focus-input100" data-placeholder="{{ __('Nieuw wachtoord') }}"></span>
                    </div>

                    {{-- Herhaling Wachtwoord --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Voer een wachtwoord in') }}">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input id="passowrd-confirm" class="input100" type="password" name="password_confirmation"
                            required autocomplete="new-password">
                        <span class="focus-input100" data-placeholder="{{ __('Herhaal Wachtoord') }}"></span>
                    </div>

                    {{-- Reset password button --}}
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit">
                                {{ __('Verander Wachtwoord') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</body>

</html>