<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.head')
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">

                {{-- Bericht --}}
                <span class="login100-form-title p-b-50">
                    {{ __('Wachtwoord vergeten') }}
                </span>

                {{-- Email sent verification --}}
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif

                {{-- Errors --}}
                @if ($errors->any())
                <div class="alert alert-danger invalid-input">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="login100-form validate-form" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    {{-- Email --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Onjuiste email') }}">
                        <input id="email" class="input100" type="text" name="email" autocomplete="email"
                            value="{{ old('email') }}" autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    {{-- Resend email button --}}
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit">
                                {{ __('Wachtwoord aanvragen') }}
                            </button>
                        </div>
                    </div>

                    {{-- Overige --}}
                    <div class="text-center p-t-115">
                        <span class="txt1">
                            {{ __('Wachtwoord toch niet vergeten?') }}
                        </span>

                        <a class="txt2" href="{{ route('login') }}">
                            {{ __('Log in.') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>