<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.head')
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                    @csrf

                    {{-- Bericht --}}
                    <span class="login100-form-title p-b-50">
                        Welkom
                    </span>

                    {{-- Errors --}}
                    @if ($errors->any())
                    <div class="alert alert-danger invalid-input">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- Email --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Onjuiste email') }}">
                        <input id="email" class="input100 @error('email') is-invalid @enderror" type="text" name="email"
                            autocomplete="email" value="{{ old('email') }}" autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>


                    {{-- Wachtwoord --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Voer een wachtwoord in') }}">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input id="passowrd" class="input100" type="password" name="password">
                        <span class="focus-input100" data-placeholder="{{ __('Wachtwoord') }}"></span>
                    </div>

                    {{-- Remember me checkbox --}}
                    <div class="container-login100-form">
                        <div class="wrap-login100-form">
                            <div class="pretty p-icon p-rotate">
                                <input type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }} />
                                <div class="state p-success">
                                    <i class="icon mdi mdi-check"></i>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Gegevens onthouden') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Login button --}}
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit">
                                {{ __('Inloggen') }}
                            </button>
                        </div>
                    </div>

                    {{-- Overige --}}
                    <div class="text-center p-t-115">
                        <span class="txt1">
                            {{ __('Heeft u geen account?') }}
                        </span>

                        <a class="txt2" href="{{ route('register') }}">
                            {{ __('Meld u hier aan!') }}
                        </a>
                    </div>
                    <div class="text-center">
                        <span class="txt1">
                            {{ __('Wachtwoord vergeten?') }}
                        </span>

                        <a class="txt2" href="{{ route('password.request') }}">
                            {{ __('Opnieuw aanvragen.') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>