<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.head')
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="{{ route('register') }}"
                    enctype="multipart/form-data">
                    @csrf

                    {{-- Bericht --}}
                    <span class="login100-form-title p-b-50">
                        {{ __('Meld u hier aan!') }}
                    </span>

                    {{-- Errors --}}
                    @if ($errors->any())
                    <div class="alert alert-danger invalid-input">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- Naam --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Vul uw naam in') }}">
                        <input id="name" class="input100" type="text" name="name" value="{{ old('name') }}" autofocus>
                        <span class="focus-input100" data-placeholder="{{ __('Naam') }}"></span>
                    </div>

                    {{-- Email --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Onvolledige email') }}">
                        <input id="email" class="input100 @error('email') is-invalid @enderror" type="text" name="email"
                            autocomplete="email" value="{{ old('email') }}" autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    {{-- Wachtwoord --}}
                    <div class="wrap-input100 validate-input" data-validate="{{ __('Voer een wachtwoord in') }}">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input id="password" class="input100 @error('password') is-invalid @enderror" type="password"
                            name="password">
                        <span class="focus-input100" data-placeholder="{{ __('Wachtwoord') }}"></span>
                    </div>

                    {{-- Wachtwoord confirm --}}
                    <div class="wrap-input100 validate-input" data-validate="Voer een wachtwoord in">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input class="input100 @error('password-confirm') is-invalid @enderror" id="password-confirm"
                            type="password" class="form-control" name="password_confirmation"
                            autocomplete="new-password">
                        <span class="focus-input100" data-placeholder="{{ __('Herhaal Wachtwoord') }}"></span>
                    </div>

                    {{-- Aanmeld button --}}
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit">
                                {{ __('Aanmelden') }}
                            </button>
                        </div>
                    </div>

                    {{-- Overige --}}
                    <div class="text-center p-t-115">
                        <span class="txt1">
                            {{ __('Heeft u al een account?') }}
                        </span>

                        <a class="txt2" href="{{ route('login') }}">
                            {{ __('Log in.') }}
                        </a>
                    </div>

                </form>
            </div>
        </div>
    </div>

</body>

</html>