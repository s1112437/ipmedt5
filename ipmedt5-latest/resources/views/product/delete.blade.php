@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="/css/producten.css">
  <form class="delete" action="/product/{{$product->id}}" method="POST">
    {{csrf_field()}}
    {{ method_field('DELETE') }}
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-3">
              <div class="card" style="width: 16rem;">
                  <img class="card-img-top" alt="{{$product->name}}" src="/images/producten/{{$product->image}}" style="padding: 10px 10px 21px 10px";/>
                  <div class="card-header">
                      <p class="card-text"><b>Definitief verwijderen?</b></p>
                      <button type="submit" class="btn btn-outline-danger">Verwijder</button><a href="/producten" class="btn btn-outline-success pull-right">Annuleer</a>
                  </div>
              </div>
          </div>
        </div>
      </div>
  </form>
@endsection
