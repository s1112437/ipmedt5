@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="list-group">
                    <a class="list-group-item list-group-item-action"  href="/product/create/ir">
                        Product toevoegen middels Visual Recognition
                    </a>
                    <a class="list-group-item list-group-item-action" href="/product/create/bc">
                        Product toevoegen middels Barcode
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection