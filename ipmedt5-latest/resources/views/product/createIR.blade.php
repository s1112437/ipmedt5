@extends('layouts.app')


@section('content')
<script src="{{ asset('js/camera.js') }}" defer></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div id="js--card" style="background: #fbfbfb;
    border-radius: 0.15em;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    float: left;
    font-size: 16px;
    left: 50%;
    margin: 0 auto;
    padding: 24px 32px;
    position: absolute;
    text-align: center;
    top: 400px;
    -webkit-transform: translateY(-50%) translateX(-50%);
    transform: translateY(-50%) translateX(-50%);
    width: 320px;
    z-index: 3000;
    display: none;">
    <img src="/images/icons/preloader.gif" style="width: 50px; height: 50px;"/>
    <div id="js--cardText" style="margin: 15px 0; width: 100%;">Uploaden...</div>
    
</div>
@isset($errmsg)
<div id="js--errorCard" style="background: #fbfbfb;
    border-radius: 0.15em;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    float: left;
    font-size: 16px;
    left: 50%;
    margin: 0 auto;
    padding: 24px 32px;
    position: absolute;
    text-align: center;
    top: 400px;
    -webkit-transform: translateY(-50%) translateX(-50%);
    transform: translateY(-50%) translateX(-50%);
    width: 400px;
    z-index: 3000;">
    <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
    <div style="margin: 15px 0; width: 100%; color: red">{{ $errmsg }}</div>
    
</div>
<script>
setTimeout(() => {
    document.getElementById('js--errorCard').style.display = 'none';
}, 5000)
</script>
@endisset
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="/product/create/ir" id="js--irForm" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{-- <div class="wrap-input100">
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" name="picture" onchange="uploadFile(event);" class="custom-file-input" id="inputGroupFile01">
                                    <label id="js--fileName" class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                            </div> --}}
                            <button id="js--take" type="button" class="btn btn-outline-primary btn-lg btn-block">
                                
                                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
                                <div>Maak een foto</div>
                            </button>
                            <div id='js--cameraBox'>
                                <div id="js--videoOverlay" style="width: 688px; height: 516px; background-color: rgba(0,0,0,0.6); position: absolute; display: none; align-items: center;justify-content: center;">
                                <span id="js--videoOverlayText" style="color: white; font-size: 3em"> 5 </span>
                                <input id="js--picture" type="hidden" name="image">
                                </div>
                                <video id="vid-show" style="width: 688px; height: 516px;" autoplay></video>
                                
                            </div>
                            <input id="js--pictureURL" type="hidden" name="pictureURL" value="">
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>

@endsection
