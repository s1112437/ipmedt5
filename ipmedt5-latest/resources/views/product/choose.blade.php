@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <p>Selecteer het product dat u wilt toevoegen</p>
                    <form method="post" action='{{ route('choose', ['translationsProducts' => $products , 'translationsCategories' => $categories , 'filename' => $filename]) }}'>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                    <ul name="product" class="list-group">
                        @foreach($products as $product)
                        <button type="submit" name='id' value="{{ $loop->index }}" name='id' class="list-group-item">{{ $product['translation'] }}</button>
                        @endforeach
                    </ul>
                </form>
                <a type="button" href="/product/create/ir" style="width: 100%; margin-top: 10px;" class="btn btn-danger">Geen van bovenstaande</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
