@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" alt="koelkast met kruis" src="/images/nietgeschikt.jpeg";/>
                <div class="card-header">
                    <h4 class="card-text">Niet geschikt!</h4>
                    <p>Bewaar {{$product}} buiten de koelkast.</p>
                    <a href="/product/create/ir" type="button" class="btn btn-outline-primary">Nieuw product</a><a href="/producten" type="button" class="btn btn-outline-secondary pull-right">Alle producten</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
