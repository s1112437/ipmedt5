@extends('layouts.app')

@php
    use App\Http\Controllers\ProductController;
@endphp

@section('content')
<script>
    window.onload = () => {
        // Dit stuk code gaat stuk als je het in een los JS bestand zet
        // Vaste variables
        const BASE_URL = "https://api.voedingscentrum.nl/api/bewaarwijzer/products/"

        // Haal productnaam uit Laravel - Dit werkt enkel in BLADE!
        var product_naam = {!! json_encode($product->name) !!};
        // Activeren functie
        getProductName(product_naam);

        function getProductName(product) {
            fetch(BASE_URL + product).then(function(response) {
                return response.json()
            }).then(function(body) {
                //console.log(body[0]);
                $('#sense').html(body[0]['Senses']);
                $('#tip').html(body[0]['Tip']);
                $('#product-bewaar').html("Bewaartips voor: " + body[0]['Name']);
                $('#product-sense').html("Is mijn " + body[0]['Name'] + " bedorven?");
            });
        };
    }
</script>
<form role="form" enctype="multipart/form-data" action="/product/{{$product->id}}/patch" method="POST">
    @csrf
    {{ method_field('PATCH') }}
    <div class="jumbotron product-header">
        <div class="container">
            <div class="row">
                <div class="column">
                    <img alt="{{$product->name}}" src="/images/producten/{{$product->image}}" class="product-image">
                </div>
                <div class="column display-3" style="padding-left: 20px;">
                    <h1>{{$product->name}}</h1>
                    <p><b>Houdbaar tot:</b> <input class="form-control" type="date" name="datum" value=<?php ProductController::getDatePickerFormat($product->expiration_date) ?>> <br>
                    <b>Categorie:</b> {{$product->category}}<br><br>
                    @if (!is_null($product->expiration_date)) <i>De houdbaarheidsdatum is gegenereerd op basis van het advies van het voedingscentrum.</i></p> @endif
                </div>
                <div class="column">
                    <br>
                    <a href="/product/{{$product->id}}"><button type="button" class="btn btn-danger pull-right ah-button" style="margin-bottom: 20px;">Annuleer</button></a>
                    <br>
                    <button type="submit" class="btn btn-success pull-right ah-button">Aanpassing opslaan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <h5 class="card-header" id="product-sense"></h5>
                    <div class="card-body">
                        <p class="card-text" id="sense"></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <h5 class="card-header" id="product-bewaar"></h5>
                    <div class="card-body">
                        <p class="card-text" id="tip"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
