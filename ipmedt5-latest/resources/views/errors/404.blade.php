@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Deze pagina is niet in gebruik.</div>
                <div class="card-body">
                    <a href="/home" type="button" class="btn btn-success">Naar dashboard</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
