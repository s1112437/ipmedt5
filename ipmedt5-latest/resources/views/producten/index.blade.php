@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row right" style="margin-right:20px;">
        <form action="/category/filter" method="GET" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="input-group">
                <label for="filter" class="filter-label">Filter op categorie:</label>
                <select class="filter form-control" onchange="this.form.submit()" id="filter" name="filter">
                    <option value="alle categorieen">alle categorieen</option>
                    @foreach($categories as $categorie)
                        <option value="{{$categorie->category}}">{{$categorie->category}}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>
    <div class="row" style="margin-left:10px;margin-right:10px;">
        <p class="lead">Scan een product met de barcode-scanner om deze toe te voegen</p>
        <p>Klik op het camera-icoontje rechts onderin om een product toe te voegen met behulp van de webcam</p>
    </div>
    <div class="row">
        @include('layouts.products')
    </div>
</div>
<a href="/product/create/ir"><div class="fab fa fa-camera-retro fa-3x"></div></a>

<div class="toast" id="test"  style="position: fixed; bottom: 150px; right: 0;" role="alert" aria-live="assertive" aria-atomic="true"  data-autohide="false">
    <div class="toast-header">
        <svg class="rounded mr-2" style="width:24px;" viewBox="0 0 24 24" id="ah-logo"><path d="M22.55 11.09L17.67 2A2.61 2.61 0 0 0 16 .6a2.47 2.47 0 0 0-.7-.1 2.78 2.78 0 0 0-1.3.4L5 5.88a3 3 0 0 0-1.38 1.78l-2.41 8.39A2.55 2.55 0 0 0 3 19.33l14.25 4.05a2.41 2.41 0 0 0 3.16-1.76l2.35-8.26a3.07 3.07 0 0 0-.21-2.27z" fill="#fff"></path><path d="M21.63 11.57l-4.77-8.9a1.88 1.88 0 0 0-2.58-.76L5.54 6.78A2 2 0 0 0 4.61 8l-2.34 8.1a1.83 1.83 0 0 0 1.23 2.28l13.77 3.92a1.8 1.8 0 0 0 2.25-1.25l2.28-8a2.07 2.07 0 0 0-.17-1.48z" fill="#00ade6"></path><path d="M12.87 10.81c.77-1.11 1.48-2.22 2.79-2.22a2.25 2.25 0 0 1 2.25 2.24v6.55h-1.64v-6.2c0-.85-.69-.85-.69-.85-.56 0-1.57 1.38-2.7 2.92v4.13h-1.66v-1.91s-1.09 1.92-2.73 1.92C6.64 17.39 6 16.11 6 13.06S6.42 8.6 8.4 8.6c1.51 0 2.81 2.2 2.81 2.2V9.4l1.66-2.23s-.01 3.65 0 3.64zm-1.95 2.47s-1.51-2.95-2.49-2.95c-.76 0-.83.8-.83 2.73s.11 2.69.82 2.69c.97-.01 2.5-2.47 2.5-2.47z" fill="#fff"></path></svg>
        <strong id="js--toastAHTitle" class="mr-auto">Boodschappen in winkelwagentje</strong>
        <small class="text-muted"></small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">
        <div>Er zitten boodschappen in uw AH Winkelwagentje klik op bestellen om de bestelling te plaatsen of op bekijk winkelwagentje om uw winkelwagentje te bekijken</div>
        <a class="btn btn-primary btn-sm" href="https://www.ah.nl/mijnlijst" target="_blank" style="float: left;margin: 10px 0;" role="button"><svg viewBox="0 0 24 24" width="16" height="16" stroke="white" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg> Bekijk winkelwagentje</a>
        <a class="btn btn-primary btn-sm" href="https://ah.nl/bestellen" target="_blank" style="float: right;margin: 10px 0;" role="button"><svg viewBox="0 0 24 24" width="16" height="16" stroke="white" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg> Bestellen</a>
    </div>
</div>
<div id="js--errorCardAH" style="background: #fbfbfb;
    border-radius: 0.15em;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    float: left;
    font-size: 16px;
    left: 50%;
    margin: 0 auto;
    padding: 24px 32px;
    position: absolute;
    text-align: center;
    top: 400px;
    -webkit-transform: translateY(-50%) translateX(-50%);
    transform: translateY(-50%) translateX(-50%);
    width: 400px;
    z-index: 3000;
    display: none;">
    <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
    <div style="margin: 15px 0; width: 100%; color: red">Controleer of de AH Token nog geldig is!</div>
</div>
<script>
    $(document).ready(function(){
        $('#test').toast('hide');
        pingCart()
    });

    window.onload = () => {
        var field = 'added';
        var url = window.location.href;
        if(url.indexOf('?' + field + '=yes') != -1) {
            setTimeout(function() { 
                Swal.fire(
                    'Het is gelukt!',
                    'Een product is toegevoegd aan je overzicht',
                    'success'
                )
            }, 500);
        } else {
            //Doen we lekker niks :)
        }
    }
</script>
@endsection
