@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row right" style="margin-right:20px;">
        <form action="/category/filter" method="GET" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="input-group">
                <label for="filter" class="filter-label">Filter op categorie:</label>
                <select class="filter form-control" onchange="this.form.submit()" id="filter" name="filter">
                    <option value="alle categorieen">alle categorieen</option>
                    @foreach($categories as $category)
                        @if($category->name == $filter)
                            <option selected value="{{$category->name}}">{{$category->name}}</option>
                        @else
                            <option value="{{$category->name}}">{{$category->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </form>
    </div>
    <div class="row" style="margin-left:10px;margin-right:10px;">
        <p class="lead">Scan een product met de barcode-scanner om deze toe te voegen</p>
        <p>Klik op het camera-icoontje rechts onderin om een product toe te voegen met behulp van de webcam</p>
    </div>
    <div class="row">
        @include('layouts.products')
    </div>
</div>
<a href="/product/create/ir"><div class="fab fa fa-camera-retro fa-3x"> + </div></a>
@endsection
