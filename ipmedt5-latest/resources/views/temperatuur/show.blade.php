@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-6">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-text">Temperatuur</h4>
                  </div>
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>Temperatuur in Celsius:</div>
                          <span id="temperature" class="badge badge-primary badge-pill"></span>
                      </li>
                  </ul>

                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>Gevoelstemperatuur:</div>
                          <span id="heat_index" class="badge badge-primary badge-pill"></span>
                      </li>
                  </ul>
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>Bewerkt op:</div>
                          <span class="badge badge-secondary badge-pill updated"></span>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="col-md-6">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-text">Luchtvochtigheid</h4>
                  </div>
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>Luchtvochtigheid:</div>
                          <span id="humidity" class="badge badge-primary badge-pill"></span>
                      </li>
                  </ul>
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>Bewerkt op:</div>
                          <span  class="badge badge-secondary badge-pill updated"></span>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="col-md-6">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-text">Advies temperatuur</h4>
                  </div>
                  <div class="card-body">
                      <p>Vanuit gezondheidsoverwegingen is het advies om de temperatuur van een koelkast in te stellen tussen de 3 °C en 4 °C. Deze temperaturen remmen de groei van bacteriën en schimmels het beste.<br><br>

                      Wanneer de koelkast te warm staat afgesteld en de temperatuur boven de 7°C komt bederft je voedsel. </p>
                  </div>
              </div>
          </div>
          <div class="col-md-6">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-text">Advies luchtvochtigheid</h4>
                  </div>
                  <div class="card-body">
                      <p>Uitdrogen is een belangrijke reden waarom verse levensmiddelen minder lang bewaard kunnen worden. Als consument kun je iets doen om een hogere luchtvochtigheid te krijgen.<br><br>
                      Sluit een vochtig product goed af! Bijvoorbeeld door gebruik te maken van de groentelade, afsluiten van kaas en vleeswaren in een aparte doos en bewaren van eieren in de originele verpakking.</p>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('javascript')
    @parent
    <script>
        fill();
        setInterval(fill, 5000);
    </script>
@endsection
