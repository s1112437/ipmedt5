@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8">
                <form action="" class="card card-sm">
                    <div class="card-body row no-gutters align-items-center">
                        <div class="col">
                            <input type="search" class="form-control form-control-lg form-control-borderless" id="ingredient" placeholder="Voer een ingredient in">
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-lg btn-success" type="button" id="gerecht-button" onclick="getGerechten(document.getElementById('ingredient').value)">Zoeken</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="card-gerechten"></div>
    </div>
@endsection