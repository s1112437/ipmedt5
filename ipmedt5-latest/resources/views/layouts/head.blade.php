<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Koelkast checker">
<link rel="icon" type="image/png" href="{{ asset('images/icons/favicon.ico') }}" />

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name') }}</title>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/master.js') }}" defer></script>
<script src="{{ asset('js/inloggen.js') }}" defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="{{ asset('js/animsition/js/animsition.min.js') }}" defer></script>
<script src="{{ asset('js/select2/select2.min.js') }}" defer></script>
<script src="{{ asset('js/daterangepicker/moment.min.js') }}" defer></script>
<script src="{{ asset('js/daterangepicker/daterangepicker.js') }}" defer></script>
<script src="{{ asset('js/countdowntime/countdowntime.js') }}" defer></script>
<script src="{{ asset('js/uploadFile.js') }}" defer></script>
<script src="{{ asset('js/orderByAH.js') }}" defer></script>
<script src="{{ asset('js/gerechten/gerechtenPagina.js') }}" defer></script>
<script src="{{ asset('js/gerechten/gerechtenHome.js') }}" defer></script>
<script src="{{ asset('js/notifications.js') }}" defer></script>


<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->

<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
<script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-storage.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/iconic/css/material-design-iconic-font.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/css-hamburgers/hamburgers.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/animsition/css/animsition.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/producten.css') }}">
<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyCF0fK_DI79tZpjSdrJ2bGNpLb_gYnePx4",
      authDomain: "slimme-koelkast.firebaseapp.com",
      databaseURL: "https://slimme-koelkast.firebaseio.com",
      projectId: "slimme-koelkast",
      storageBucket: "slimme-koelkast.appspot.com",
      messagingSenderId: "41704803047",
      appId: "1:41704803047:web:081ee4b5796e0eed1d7042"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  </script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/settings.css') }}">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
