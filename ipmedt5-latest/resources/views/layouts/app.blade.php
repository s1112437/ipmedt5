<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.head')
</head>

<body>
    <div id="app">
        {{-- Nav --}}
        @include('layouts.nav')

        {{-- Content --}}
        <main id="dashboard" class="py-4 dashboard">
            @yield('content')
        </main>

        {{-- Footer --}}
        <footer>
            @yield('layouts.footer')
        </footer>

    </div>

</body>

@section('javascript')
    <script>
        var temperature = document.getElementById('temperature');
        var humidity = document.getElementById('humidity');
        var heat_index = document.getElementById('heat_index');
        var updated = document.getElementsByClassName('updated');
        var indicator = document.getElementById('indicator');

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function nowFormated() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            return h + ":" + m + ":" + s;
        }

        var fill = function () {

            const request = new Request('/api/temperatuur');

            fetch(request)
                .then(response => {
                    response.json().then(data => {
                        if (temperature != null) {
                            temperature.innerHTML = data['temperature'] + ' ºC';
                            if(data['temperature'] >=7){
                                temperature.classList.replace('badge-primary', 'badge-danger');
                                temperature.style.color.replace('green', 'red');
                            }else{
                                temperature.classList.replace('badge-danger', 'badge-primary');
                                temperature.style.color.replace('red', 'green');
                            }
                        }

                        if (humidity != null) {
                            humidity.innerHTML = data['humidity'] + ' %';
                        }

                        if (heat_index != null) {
                            heat_index.innerHTML = data['heat_index'] + ' ºC';
                            if(data['heat_index'] >=7){
                                heat_index.classList.replace('badge-primary', 'badge-danger');
                            }else{
                                heat_index.classList.replace('badge-danger', 'badge-primary');
                            }
                        }

                        for (var i = 0; i < updated.length; i++) {
                            updated[i].innerHTML = nowFormated();
                        }

                        if (indicator != null) {
                            if(data['temperature'] >=7){
                                indicator.innerHTML = 'Te hoog!';
                                indicator.classList.replace('badge-success', 'badge-danger');
                            }else{
                                indicator.innerHTML = 'Goed!';
                                indicator.classList.replace('badge-danger', 'badge-success');
                            }
                        }
                    });

                });


        };
    </script>
@show

</html>
