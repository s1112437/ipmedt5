<?php
    function active($currect_page){
        $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
        $url = end($url_array);
        if($currect_page == $url){
            echo 'active'; //class name in css
        }
    }
?>

<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <!-- Home -->
                <li class="menu-item <?php active('home');?>">
                    <a href="{{ route('home') }}"><i class="menu-icon fa fa-laptop"></i>{{ __('Home') }} </a>
                </li>
                <!-- Mijn Koelkast -->
                <li class="menu-title">{{ __('Mijn koelkast') }}</li>
                <li class="menu-item <?php active('producten');?>">
                    <a href="{{ route('producten') }}"><i class="menu-icon fa fa-inbox"></i>{{ __('Producten') }} </a>
                </li>
                <li class="menu-item <?php active('temperatuur');?>">
                    <a href="{{ route('temperatuur') }}"><i class="menu-icon fa fa-thermometer-three-quarters"></i>{{ __('Temperatuur') }} </a>
                </li>
                <li class="menu-item <?php active('gerechten');?>">
                    <a href="{{ route('gerechten') }}"><i class="menu-icon fa fa-birthday-cake"></i>{{ __('Mogelijke gerechten') }} </a>
                </li>
            </ul>
        </div>
    </nav>
</aside>
<div id="right-panel" class="right-panel">
    <header id="header" class="header">
        <div class="top-left">
            <div class="navbar-header">
                <a class="navbar-brand">{{ config('app.name', 'Ipmedt5') }}</a>
                <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
            </div>
        </div>
        <div class="top-right">
            <div class="header-menu">
                <div class="header-left">
                    {{-- Als er niet ingelogd is --}}
                    @guest
                    <div class="nav-item">
                        <a class="" href="{{ route('login') }}">{{ __('Inloggen') }}</a>
                    </div>
                    @if (Route::has('register'))
                    <div class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Aanmelden') }}</a>
                    </div>
                    @endif
                    {{-- Als er wel ingelogd is --}}
                    @else
                    <button class="search-trigger"><i class="fa fa-search"></i></button>
                    <div class="form-inline">
                        <form class="search-form">
                            <input class="form-control mr-sm-2" type="text" placeholder="Search ..."
                                aria-label="Search">
                            <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                        </form>
                    </div>
                    <div class="dropdown for-notification">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="notification"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            @if (auth()->user()->notifications->count() < 1)
                                <span class="count bg-success">{{auth()->user()->notifications->count()}}</span>
                            @else
                                <span class="count bg-danger">{{auth()->user()->notifications->count()}}</span>
                            @endif
                        </button>
                        <div class="dropdown-menu" style="min-width: 350px;" aria-labelledby="notification">
                            <div class="row">
                                <div class="col-md-7">
                                    <p class="red">Je hebt {{auth()->user()->notifications->count()}} meldingen</p>
                                </div>
                                <div class="col-md-5">
                                    <form method="POST" action="{{ route('deleteNotif') }}">
                                        @csrf
                                        <button type="submit"><p class="text-primary">Alles Gelezen</p></button>
                                    </form>
                                </div>
                            </div>
                            @foreach (auth()->user()->notifications as $notification)
                                @if (auth()->user()->notifications->count() < 1)
                                <a class="dropdown-item media" href="#">
                                    <i class="fa fa-check"></i>
                                    <p>Geen meldingen!</p>
                                </a>
                                @elseif ($notification->unread())
                                <a class="dropdown-item media" href="#">
                                    <i class="fa fa-warning"></i>
                                    <p>{{ $notification->data['data'] }}</p>
                                    {{-- <div class="col">
                                    <form action="{{ route('deleteSingularNotif') }}"></form>
                                        <button type="button" class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div> --}}
                                </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" v-pre>
                            <img class="user-avatar rounded-circle"
                                src="/uploads/avatars/{{ Auth::user()->avatar ?? 'default.jpg' }}" alt="User avatar" />
                        </a>
                        <div class=" user-menu dropdown-menu">
                            <a class="nav-link" href="{{ route('show') }}"><i class="fa fa- user"></i>Mijn profiel</a>
                            <a class="nav-link" href="{{ route('settings') }}"><i
                                    class="fa fa -cog"></i>{{ __('Instellingen') }}</a>

                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                <i class="fa fa-power -off"></i>{{ __('Uitloggen') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    @endguest
                </div>
            </div>
        </div>
    </header>
</div>
