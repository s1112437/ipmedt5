@extends('layouts.app')

@section('content')
<div class="container emp-profile">
    <div class="row">
        <div class="col-md-4">
            <div class="profile-img">
                <img src="/uploads/avatars/{{ Auth::user()->avatar ?? 'default.jpg' }}" alt="User avatar" />
            </div>
        </div>
        <div class="col-md-5">
            <div class="profile-head">
                <h5>
                    {{ Auth::user()->name ?? 'Naam' }}
                </h5>
                <h6>
                    Koelkast gebruiker
                </h6>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Over mij</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col">
            <a class="btn btn-primary" href="{{ route('settings') }}">Profiel aanpassen</a>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="tab-content profile-tab" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Naam</label>
                        </div>
                        <div class="col-lg-5">
                            <p>{{ Auth::user()->name ?? 'Naam' }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Email</label>
                        </div>
                        <div class="col-lg-5">
                            <p>{{ Auth::user()->email ?? 'Email' }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>AH Token</label>
                        </div>
                        <div class="col-lg-5">
                            <p>{{ Auth::user()->AHtoken ?? 'AHtoken' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection