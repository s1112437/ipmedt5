@extends('layouts.app')

@section('content')
<form role="form" enctype="multipart/form-data" action="/profile/settings" method="POST">
    @csrf
    {{ method_field('PUT') }}
    <div class="container emp-profile">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">
                    <img src="/uploads/avatars/{{ Auth::user()->avatar ?? 'default.jpg' }}" alt="User avatar" />
                    <div class="file btn btn-lg btn-primary">
                        <span>Avatar aanpassen</span>
                        <input type="file" name="avatar" class="form-control-file">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="profile-head">
                    <h5>
                        {{ Auth::user()->name ?? 'Naam' }}
                    </h5>
                    <h6>
                        Koelkast gebruiker
                    </h6>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">Over mij</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col"></div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row mb-1">
                            <div class="col-md-4">
                                <label>Naam</label>
                            </div>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="name" value="{{ Auth::user()->name ?? 'Naam' }}">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-md-4">
                                <label>Email</label>
                            </div>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="email" value="{{ Auth::user()->email ?? 'Email' }}">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-md-4">
                                <label>AH Token</label>
                            </div>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="AHtoken" value="{{ Auth::user()->AHtoken ?? 'AHtoken' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-lg-6">
                        <a href="{{route('show')}}" class="btn btn-default">Annuleren</a>
                        <span></span>
                        <button type="submit" class="btn btn-primary">Wijzigingen behouden</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
