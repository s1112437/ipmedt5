@php
    use App\Http\Controllers\ProductController;
@endphp

@extends('layouts.app')
<!--
- Vergelijk datum van producten met datum van nu en 7 dagen terug
- Toon 1 recept per productsoort wat in de range van hierboven valt
-->
@section('content')

<script src="js/gerechten/gerechtenZoeken.js"></script>
<div class="container">
    <!-- @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif -->
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-text">(Bijna) over datum</h4>
                </div>
                <div class="scroll">
                  @foreach($producten as $product)
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          <div>{{$product->name}}</div>
                          <span class="badge badge-primary badge-pill">@if (is_null($product->expiration_date)) Onbekend @else @php ProductController::getRightDateFormat($product->expiration_date) @endphp @endif</span>
                      </li>
                  </ul>
                  @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-text">Temperatuur</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div>
                          <h2 style="color: green"><b id="temperature"></b></h2>
                          <p style="margin-bottom: 0rem">In Celsius</p>
                        </div>
                        <div>
                          <h2 style="color: green"><b id="humidity"></b></h2>
                          <p style="margin-bottom: 0rem">Luchtvochtigheid</p>
                        </div>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div>Status temperatuur:</div>
                        <span id="indicator" class="badge badge-success badge-pill"></span>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div>Laatst bijgewerkt:</div>
                        <span class="badge badge-secondary badge-pill updated"></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <h3 class="text-center">Maak deze gerechten van de producten die (bijna) over datum zijn</h3>
        </div>
        <div class="row" id="card-gerechten-home" style="margin-left:15px;margin-right:15px;">
            <div id="goed-bezig" style="margin-top: 20px;padding: 50px 0;">
                <h4 class="text-center">Geen gerechten beschikbaar want er is nog niks (bijna) over datum, goed bezig!</h4>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
    @parent
    <script>
        fill();
        setInterval(fill, 5000);
    </script>
@endsection
