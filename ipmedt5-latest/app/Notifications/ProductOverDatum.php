<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductOverDatum extends Notification
{
    use Queueable;
    protected $productNaam;
    protected $productID;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($productNaam, $productID)
    {
        $this->productNaam = $productNaam;
        $this->productID = $productID;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $zin = 'Let op! Dit product is bijna over datum: ';
        $data = $zin . $this->productNaam;

        return [
            // 'product' => $this->input,
            'data' => $data
        ];
    }
}
