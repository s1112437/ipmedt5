<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LarashopAdminResetPassword extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reset uw wachtwoord')
            ->line('U ontvangt deze email omdat we een aanvraag hebben gekregen om uw wachtwoord te resetten.')
            ->action('Reset Wachtwoord', url(config('app.url').config('app.port')."/password/reset/{$this->token}"))
            ->line('Als u dit niet zelf heeft aangevraagd, hoeft u niets te doen.');
    }
    // action hierboven was van origine: route('password.reset.token', ['token' => $this->token])) maar dit werkt alleen met domeinnamen

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
