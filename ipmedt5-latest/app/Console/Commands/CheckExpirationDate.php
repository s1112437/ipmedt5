<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;

class CheckExpirationDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkexpirationdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks expiration date and calculates when to send notification to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product = Product::find(1);
        $user = $product->user;
        dd($user->name);
    }
}
