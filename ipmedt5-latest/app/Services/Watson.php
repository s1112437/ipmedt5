<?php


namespace App\Services;


class Watson {
    private $app;

    public function __constructor($app) {

    }

    public function imageRequest($pictureUrl) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.us-south.visual-recognition.watson.cloud.ibm.com/instances/bdc4ff60-24f5-45ff-84eb-82c8c42a83b0/v3/classify?url=" . urlencode($pictureUrl) . "&options=399,q85&version=2018-03-19&classifier_ids=food&threshold=0.5",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic " . config('app.ibm_ir_api')
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        return $response;
    }

    public function translate($text) {
        $json = array("text"=>$text, "model_id"=>"en-nl");

        $json = json_encode($json);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/3bb142f7-0cc3-4e04-a350-1fa04737d2e8/v3/translate?version=2018-05-01",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic " . config('app.ibm_translate_api')
            ),
        ));


        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        return $response;
    }

}
