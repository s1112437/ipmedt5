<?php


namespace App\Services;


class Barcode
{
    private $app;


    # Digit Eyes Code to Generate and Output JSON
    public function barcode($upc_code)
    {
       #****Dynamic UPC from form****

        $auth_key = env('DIGIT_EYES_AUTH_KEY');
        $app_key = env('DIGIT_EYES_APP_KEY');

        $signature = base64_encode(hash_hmac('sha1', $upc_code, $auth_key, $raw_output = true));

        #***Dynamic UPC Production***

        //$url = 'https://digit-eyes.com/gtin/v2_0/?upc_code='.$_GET["upc"].'&app_key='. $app_key
        // //.'&language=en&field_names=all&signature=' . $signature . '';
        #***Hard Code UPC Test***

        $url = 'https://digit-eyes.com/gtin/v2_0/?upc_code=' . $upc_code . '&app_key=' . $app_key
            . '&language=en&field_names=all&signature=' . $signature . '';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);

        curl_close($ch);

        $data = json_decode($data);

        if ($data->gcp == null) {
            return null;
        }

        return $data;


    }
}
