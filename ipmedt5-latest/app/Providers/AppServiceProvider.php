<?php

namespace App\Providers;

use App\Services\Barcode;
use App\Services\Watson;
use DemeterChain\B;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Watson::class, function ($app) {
            return new Watson($app);
        });
        $this->app->bind( Barcode::class, function ($app) {
            return new Barcode();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
