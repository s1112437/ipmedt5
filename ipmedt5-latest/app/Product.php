<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'expiration_date', 'category', 'image'];
  public function user() {
    return $this->belongsTo('user');
  }

  protected $table = "products";

  public function getCategory(){
      return $this->hasOne('App\Category','name','category');
  }
}
