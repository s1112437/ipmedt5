<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $table = "categories";
  protected $fillable = ['name'];

    public static function overig()
    {
        return self::where('name', '=', 'Overig')->firstOr(function () {
            return self::create([
               'name' => 'Overig'
            ]);
        });
    }

    public function getProducts(){
      return $this->hasMany('App\Product','category','name');
  }
}
