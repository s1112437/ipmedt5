<?php

namespace App\Http\Controllers;

use App\Services\Temp;
use Illuminate\Http\Request;

class TemperatuurController extends Controller
{
    public function show() {
        return view('temperatuur.show');
    }

    public function temperatuur() {
        /** @var Temp $pi */
        $pi = resolve(Temp::class);

        return json_encode($pi->temp());
    }
}
