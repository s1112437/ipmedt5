<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return view('profile/settings');
    }

    public function show()
    {
        return view('profile/show');
    }

    public function update(Request $request) {
        $userDetails = Auth::user();
        // Als avatar is geupload, sla deze op
        if($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . ',' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
            $userDetails->avatar = $filename;
            $userDetails->save();
        }
        $user = User::find($userDetails ->id);
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->AHtoken=$request->input('AHtoken');
        $user->save();
        return redirect('/profile');
    }
}
