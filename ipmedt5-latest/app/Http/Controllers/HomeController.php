<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use DB;
use App\Product;
use Auth;
use App\Notifications\ProductOverDatum;
use App\Notifications\TestNotification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $product_expiration_date = new Carbon();
        $product_expiration_date->add('4 days');
        $product_expiration_date = strtotime($product_expiration_date->toISOString());
        // auth()->user()->notify(new TestNotification);

        // $producten = User::with('products')->find(Auth::user()->id);
        $producten = Product::all()->where('user_id',Auth::user()->id);
        $filtered = $producten->where('expiration_date','<=', $product_expiration_date);

        return view('home')
            ->with('producten', $filtered);
            // ->with('producten',Product::where('expiration_date','<=', $product_expiration_date)->get());
    }

    public function getIngredients() {
        // Haal alle producten op en geef alleen de unieke soorten door aan frontend
        $products = Product::all()->where('user_id',Auth::user()->id)->unique('name');
        return json_encode($products);
    }

    public function storeNotif(Request $request) {
        $productNaam = $request->product['name'];
        $productID = $request->product['id'];
        // $huidigeOverDatum = DB::table('notifications');
        auth()->user()->notify(new ProductOverDatum($productNaam, $productID));
        // $bericht = [
        //     'fixed' => 'Binnenkort gaat dit product over datum: ',
        //     'product' => $request['name']
        // ];
    }

    public function deleteNotif() {
        DB::table('notifications')->truncate();
        return redirect('/home');
    }

}
