<?php

namespace App\Http\Controllers;

use App\Category;
use App\Notifications\BarcodeNotFoundNotification;
use App\Product;
use App\User;
use App\Services\Barcode;
use App\Services\Watson;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Str;
use Intervention\Image\Exception\NotReadableException;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Illuminate\Http\Request;
use Image;
use File;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;


class ProductController extends Controller {
    private $watson;

    public function __construct(Watson $watson) {
        $this->watson = $watson;
    }

    public function create() {
        return view('product.create');
    }

    public function createIR() {
        return view('product.createIR');
    }

    public function show($id) {
        return view('product.show')->with('product',Product::where('id','=',$id)->first());
    }
    
    public function change($id) {
        return view('product.change')->with('product',Product::where('id','=',$id)->first());
    }

    public function patch(Request $request, $id) {
        $product = Product::where('id','=',$id);
        $expiration_date = strtotime($request->input('datum'));
        // dd($expiration_date);
        $product->update(array('expiration_date' => $expiration_date));

        return redirect()->route('product', ['id' => $id]);
    }

    public function index() {
        // return view('producten.index')->with('producten',Product::all())->with('categories',Category::all());
        $producten = Product::all()->where('user_id', Auth::user()->id);
        $categories = Product::all('category')->unique('category');

        return view('producten.index')
            ->with('producten', $producten)
            ->with('categories', $categories);
    }

    public function select(Request $request){
        $id = (int)$request->id;
        var_dump($id);
        //todo: timestamp doet hij automatisch --> toegevoegd op
        $product = new Product();

        // Translate engelse productnaam naar Nederlands
        $product->name = $request->translationsProducts[$id]['translation'];

        // In geval van een foutmelding kan je zien welke naam het product heeft
        // var_dump(ucfirst($translate_name->translate($translationsProducts[0]['translation'])));

        // Haal data (productnaam) op van API voedingscentrum
        $daysInFridge = $this->voedingcentrumAPI($product->name);
        // Controleren of product wel een naam heeft
        if ($daysInFridge == null) {
            return view('product.unsuitable')->with('product',$product->name);
        } else {
            // Product image
            $product->image = $request->filename;

            // Houdbaarheids datum generen
            $epoch_expiration_date = strtotime($this->getDateExpression($daysInFridge));     // Gebruik regex om te bepalen wat de optelwaarde moet zijn
            //$human_readable_date = new DateTime("@$epoch_expiration_date");                 // Zet epoch om naar human readable datetime format
            //$human_readable_date->format('d-m-Y');              // https://api.voedingscentrum.nl/api/bewaarwijzer/products/
            $product->expiration_date = $epoch_expiration_date;

            // als de categorie bestaat, voeg de categorie aan het product toe en sla de product op
            // als de categorie niet bestaat, maak een nieuwe categorie aan, koppel die aan het product en sla
            // het product en categorie op
            $category = Category::where('name', '=', $request->translationsCategories[$id]['translation'])->get()->first();
            if($category == null) {
                $category = new Category;
                $category->name = $request->translationsCategories[$request->id]['translation'];
                $category->save();
            } else {
            }

            $product->user_id   = Auth::user()->id;
            $product->category  = $category->name;
            $product->save();

            // return redirect()->route('added', ['productName' => $product->name]);

            return redirect('/producten'.'/?'. http_build_query(['added'=>'yes']));
        }
    }

    public function IR(Request $request) {
        $pictureUrl = $request->pictureURL;

        $response = $this->watson->imageRequest($pictureUrl);

        $results = $response['images'][0]['classifiers'][0]['classes'];
        $categories = [];
        $products = [];

        foreach($results as $result) {
            if(isset($result['type_hierarchy'])){
                array_push($products, $result['class']);
            }
        }
        foreach($results as $result){
            if(isset($result['type_hierarchy'])){
                $categorie = explode('/', $result['type_hierarchy'])[1];
                array_push($categories,$categorie);
            }
        }

        $responseProducts = $this->watson->translate($products);
        $responseCategories = $this->watson->translate($categories);
        if(isset($responseProducts['translations'])){
            $translationsProducts = $responseProducts['translations'];
        }
        else{
            return view('product.createIR')->with('errmsg',"Er is iets mis gegaan, Probeer het opnieuw!");
        }
        if(isset($responseCategories['translations'])){
            $translationsCategories = $responseCategories['translations'];
        }
        else{
            return view('product.createIR')->with('errmsg',"Er is iets mis gegaan, Probeer het opnieuw!");
        }
        foreach($translationsProducts as $key=>$translationsProduct){
            $translate_name = new GoogleTranslate('nl');
            $translate_name->setSource('en');
            // $translationsProduct['translation'] = "test";
            $translationsProducts[$key]['translation'] = ucfirst($translate_name->translate($translationsProduct['translation']));
        };

        $filename = time() . '.' . 'png';
        // $filename = basename($request->pictureURL);
        Image::make($request->image)->save( public_path('../../storage/producten/' . $filename ) );
        return view('product.choose')->with('products', $translationsProducts)->with('categories', $translationsCategories)->with('filename', $filename);
        // //todo: timestamp doet hij automatisch --> toegevoegd op
        // $product = new Product();

        // // Translate engelse productnaam naar Nederlands
        // $translate_name = new GoogleTranslate('nl');
        // $translate_name->setSource('en');
        // $product->name = ucfirst($translate_name->translate($translationsProducts[0]['translation']));

        // // In geval van een foutmelding kan je zien welke naam het product heeft
        // // var_dump(ucfirst($translate_name->translate($translationsProducts[0]['translation'])));

        // // Haal data (productnaam) op van API voedingscentrum
        // $daysInFridge = $this->voedingcentrumAPI($product->name);

        // // Controleren of product wel een naam heeft
        // if ($daysInFridge == null) {
        //     return view('product.unsuitable')->with('product',$product->name);
        // } else {
        //     // Product image
        //     $image = $request->file('picture');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     Image::make($image)->save( public_path('../../storage/producten/' . $filename ) );
        //     $product->image = $filename;

        //     // Houdbaarheids datum generen
        //     $epoch_expiration_date = strtotime($this->getDateExpression($daysInFridge));     // Gebruik regex om te bepalen wat de optelwaarde moet zijn
        //     //$human_readable_date = new DateTime("@$epoch_expiration_date");                 // Zet epoch om naar human readable datetime format
        //     //$human_readable_date->format('d-m-Y');              // https://api.voedingscentrum.nl/api/bewaarwijzer/products/
        //     $product->expiration_date = $epoch_expiration_date;

        //     // als de categorie bestaat, voeg de categorie aan het product toe en sla de product op
        //     // als de categorie niet bestaat, maak een nieuwe categorie aan, koppel die aan het product en sla
        //     // het product en categorie op
        //     $category = Category::where('name', '=', $translationsCategories[0]['translation'])->get()->first();
        //     if($category == null) {
        //         $category = new Category;
        //         $category->name = $translationsCategories[0]['translation'];
        //         $category->save();
        //     } else {
        //     }

        //     $product->category = $category->name;
        //     $product->save();

        //     return redirect('/producten');
        // }
    }

    function barcode(Request $request) {

        /** @var Barcode $api */
        $api = resolve(Barcode::class);
        // Product ophalen vanuit barcode api

        $barcode = $request->barcode;

        $data = $api->barcode($barcode);

        if (is_null($data)) {
            Auth::User()->notify( new BarcodeNotFoundNotification($barcode));

            return [
                'status' => 'error',
                'message' => 'product not found'
            ];
        }

        $image = file_get_contents($data->image);
        $file_path = pathinfo($data->image);
        $file_name = Str::uuid() . '.' . $file_path['extension'];
        try {
            Image::make($image)->save( public_path('../../storage/producten/' . $file_name) );
        } catch (NotReadableException $e) {
            $file_name = 'image_not_available.jpg';
        }

        $product = new Product();
        $product->name = $data->description;
        $product->expiration_date = null;
        $product->image = $file_name;
        $product->category = Category::overig()->name;
        $product->user_id = Auth::user()->id;

        $product->save();
        // return $product;
        return [
            'status' => "success",
            'message' => 'Product added successfull'
        ];
    }

    // API call naar voedingscentrum met product naam
    function voedingcentrumAPI($productNaam) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voedingscentrum.nl/api/bewaarwijzer/products/" . $productNaam,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response, true);
        if ($response == null) {
            return null;
        } else {
            return $response[0]['DaysInFridge'];
        }
    }

    // Vormt API call voedingscentrum om naar juiste optelling van dagen, weken of maanden
    function getDateExpression($string) {
        if (preg_match('/^\d*?-?(\d*) (w|d|m)(?:ee?k|ag|aand)(?:en)?$/', $string, $matches)) {
            $dateUnitMapping = ["d" => "days", "w" => "weeks", "m" => "months"];
            return sprintf("+%d %s", $matches[1], $dateUnitMapping[$matches[2]]);
        } else {
            echo "Darn... no match :(";
        }
    }

    public function delete($id) {
        $product = Product::where('id','=',$id)->first();
        if (isset($product)) {
            return view('product.delete')->with('product',Product::where('id','=',$id)->first());
        } else {
            return view('errors.404');
        }
    }

    public function destroy($id){
        $filename = Product::select('image')->where('id','=',$id)->first();
        $filename = $filename['image'];
        if(File::exists( public_path('../../storage/producten/' . $filename))) {
            File::delete( public_path('../../storage/producten/' . $filename));
        }

        $product = Product::where('id','=',$id)->delete();

        return redirect('/producten');
    }

    public static function getRightDateFormat($timestamp) {
        $human_readable_date = new DateTime("@$timestamp");
        echo $human_readable_date->format('d-m-Y');
    }

    public static function getDatePickerFormat($timestamp) {
        $human_readable_date = new DateTime("@$timestamp");
        echo $human_readable_date->format('Y-m-d');
    }

    function addToCart($product) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ah.nl/common/api/basket/v2/add",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\"items\":[{\"quantity\":1,\"id\":". $product . "}]}",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/json",
            "cache-control: no-cache",
            "cookie: ah_token_presumed=" . Auth::User()->AHtoken . ";",
            "Access-Control-Allow-Headers: X-CSRF-Token"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
    function getCart(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ah.nl/common/api/basket/v2",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/json",
            "cache-control: no-cache",
            "cookie: ah_token_presumed=" . Auth::User()->AHtoken . ";",
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://www.ah.nl/service/rest/delegate?url=/producten/product/wi" . "/0",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 0,
        //   CURLOPT_FOLLOWLOCATION => true,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "GET",
        //   CURLOPT_HTTPHEADER => array(
        //     "Cookie: SSLB=1; TS0163d06f=01919b9b64e986549ce6bc69c06bf43254eb35f0816b11e36e947cb1aaddef86537ad4fc88ff8af76ae06c4208dc0752da1fa4255519a635a89441b6d2c16836f1b3682ab7; i18next=nl_NL; JSESSIONID=1FDFCB4B9CA98ED4CA116E92566AC09D.ahws_7; ahold_presumed_member_no=37177476_CHK%2F1ZynuFscWEOLIjlMgf%2BRA%3D%3D; ah_token_presumed=83d896ff-a8c6-45a5-ab42-9f5c45bac4be.a5c0a30b-8300-495b-951f-f9978399b4f0; ah_token=19b95aba-b3eb-4bb3-a42f-c0d6d7a34803.fd72b756-c737-44e1-aafc-0fc8267531d4; TS01fb4f52=01919b9b64bf9ad472437ed66651296cef09dad93cce7d3c48f016c9eaf26676c9ded58a532abc505fa564b16752594804caa1ec4afe73d44cf7406850c24977f0bddcd94a9e7bdebbdcaf2c63317ac979a4f4eeb5af286ae8bf752787a5ecfd4a8f4ae6fc32cc6c399799f8cb90aa7f0a673abe29ea483d44e1f32e224fe1a6f9b6b429487cf54cc5068c2c16b9357675ef95064e493e09bc7fec5b37c8ff48348b54e5f47b7ffaa5416c9e6745b0dd2a0d9dc7e1"
        //   ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // echo $response;
            }
}
