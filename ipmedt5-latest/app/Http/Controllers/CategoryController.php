<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use App\Product;
use Auth;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index() {
        return Category::all();
    }

    public function filter(Request $request){
        $category = $request->filter;
        if ($category == "alle categorieen") {
            return redirect('/producten');
        } else {
            // Filter producten op basis van user_id en laat enkel producten zien die de juiste category hebben
            $producten = Product::all()->where('user_id', Auth::user()->id);
            $filtered = $producten->where('category','=',$category);

            return view('category.producten')
                // ->with('producten',Category::where('name','=',$category)->first()->getProducts)
                ->with('producten', $filtered)
                ->with('categories',Category::all())
                ->with('filter', $category);
        }
    }
}
