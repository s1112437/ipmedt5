import json
from ibm_watson import VisualRecognitionV4
from ibm_watson.visual_recognition_v4 import AnalyzeEnums, FileWithMetadata
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

authenticator = IAMAuthenticator('t1fuJGYNJ1Ilx4sJGRYWZ9uPHSmyDzsBkS9U1_yLfbmU')
visual_recognition = VisualRecognitionV4(
    version='2019-02-11',
    authenticator=authenticator
)

visual_recognition.set_service_url('https://api.us-south.visual-recognition.watson.cloud.ibm.com/instances/bdc4ff60-24f5-45ff-84eb-82c8c42a83b0')

with open('../../../storage/app/products/product.jpg', 'rb') as product:
    result = visual_recognition.analyze(
        collection_ids=["5826c5ec-6f86-44b1-ab2b-cca6c75f2fc7"],
        features=[AnalyzeEnums.Features.OBJECTS.value],
        images_file=[
            FileWithMetadata(product)
        ]).get_result()
    print(json.dumps(result, indent=2))