window.onload = () => {
    $.getJSON('getIngredients', function(data) {
        var gerecht = document.getElementById("goed-bezig");

        // RAW data uitlezen
        console.log(data);

        // Timestamp van nu - 4 dagen
        var minutes = 1000 * 60;
        var hours = minutes * 60;
        var days = hours * 24;
        var d = new Date();
        var t = Math.round(d.getTime()/1000);
        console.log("EPOCH tijd: " + t);

        // Aantal secondes van 4 dagen
        var dagen4 = Math.round((days * 4)/1000);
        console.log("SECONDES van 4 dagen: " + dagen4);

        // EPOCH + 4 dagen
        var stampPlus4Days = (t + dagen4);
        console.log("EPOCH + 4 dagen: " + stampPlus4Days);

        // Filter de object array op producten die binnen de range vallen
        var filterData = Object.keys(data).reduce(function(r, e) {
            if (data[e]['expiration_date'] == null) return r;
            if (stampPlus4Days > data[e]['expiration_date']) r[e] = data[e];
            return r;
        }, {});
        console.log(filterData);

        // Zet alle prodcuten in 1 string
        var ingredienten = "";
        for (x in filterData) {
            $.storeNotif(data[x]);
            // displayNotif(filterData[x]['name']);
            ingredienten += data[x]['name'] + " ";

        }
        console.log("We gaan zoeken op: " + ingredienten);

        // Check of er wel iets over datum begint te raken
        if (ingredienten != "") {
            // Als de string niet leeg is ga dan opzoek naar producten
            getGerechtenHome(ingredienten);
            gerecht.style.display = "none";
        } else {
            // Anders toon melding dat er geen gerechten beschikbaar zijn
            gerecht.style.display = "display";
        }
    })
}
