function getGerechtenHome(ingredienten) {
    // BASE_URL staat al beschreven in de gerechten.js bestand

    fetch(BASE_URL + ingredienten).then(function(response) {
        return response.json()
    }).then(function(body) {
        //console.log(body['recipes']);
        for(i = 0; i < 4; i++) {
            var titel =             body['recipes'][i]['title'].replace(/\&shy;/gi, "");
            var bereidingsTijd =    body['recipes'][i]['completeTotalTime'];
            var rating =            body['recipes'][i]['averageRating'];
            var image =             body['recipes'][i]['recipeImageRenditionsByKey']['Recipe_200']['url'];
            var url =               body['recipes'][i]['href'];
            toonDataHome(titel, bereidingsTijd, rating, image, url);
        }
    });
}

// Voegt nieuw gerecht object toe op basis van API data
function toonDataHome(titel, bereidingsTijd, rating, image, url) {
    const CardSpaceHome = document.getElementById('card-gerechten-home');
    let mijnGerechtenHome = new Gerechten(CardSpaceHome);
    mijnGerechtenHome.add(titel, bereidingsTijd, rating, image, url);
}