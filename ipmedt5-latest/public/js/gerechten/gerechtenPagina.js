const BASE_URL = "https://ah.nl/allerhande/api/recipes/search?Ntt="

function getGerechten(ingredient) {
    fetch(BASE_URL + ingredient).then(function(response) {
        return response.json()
    }).then(function(body) {
        //console.log(body['recipes']);
        flush();
        for(i = 0; i < body['recipes'].length; i++) {
            var titel =             body['recipes'][i]['title'].replace(/\&shy;/gi, "");
            var bereidingsTijd =    body['recipes'][i]['completeTotalTime'];
            var rating =            body['recipes'][i]['averageRating'];
            var image =             body['recipes'][i]['recipeImageRenditionsByKey']['Recipe_200']['url'];
            var url =               body['recipes'][i]['href'];
            toonData(titel, bereidingsTijd, rating, image, url);
        }
    });
};

class Gerechten {
    constructor(ref) {
        this.list = [];
        this.hmi_ref = ref;

        // De opbouw van de DOM elementen
        this.BS = {}
        this.BS.container = document.createElement('div');
        this.BS.card      = document.createElement('div');
        this.BS.url       = document.createElement('a');
        this.BS.image     = document.createElement('img');
        this.BS.header    = document.createElement('div');
        this.BS.title     = document.createElement('h4');
        this.BS.details   = document.createElement('p');

        this.BS.header.appendChild( this.BS.title )
        this.BS.header.appendChild( this.BS.details )
        this.BS.url.appendChild( this.BS.image);
        this.BS.card.appendChild( this.BS.url );
        this.BS.card.appendChild( this.BS.header );
        this.BS.container.appendChild( this.BS.card );

        this.BS.container.className = 'col-sm-6 col-md-4 col-lg-3';
        this.BS.card.className      = 'card';
        this.BS.image.className     = 'card-img-top';
        this.BS.header.className    = 'card-header';
        this.BS.title.className     = 'card-text';
    }

    // Voegt een gerecht toe
    add ( titel, bereidingsTijd, rating, image, url) {
        this.list.push( {titel, bereidingsTijd, rating, image, url } )

        this.BS.image.src         = image;
        this.BS.title.textContent = titel;
        this.BS.details.innerHTML = `<b>Bereidingstijd: </b>${bereidingsTijd}</br><b>Beoordeling: </b>${rating} sterren`;
        this.BS.url.href          = `https://ah.nl${url}`;
        this.BS.url.target        = '_blank'

        let newNode = this.BS.container.cloneNode(true)
        this.hmi_ref.appendChild(newNode);
    }
}

// Voegt nieuw gerecht object toe op basis van API data
function toonData(titel, bereidingsTijd, rating, image, url) {
    const CardSpace = document.getElementById('card-gerechten');
    let mijnGerechten = new Gerechten(CardSpace);
    mijnGerechten.add(titel, bereidingsTijd, rating, image, url);
}

// Gooit de inhoud van de container leeg voor nieuwe zoekresultaten
function flush() {
    const CardSpace = document.getElementById('card-gerechten');
    CardSpace.innerHTML = '';
}