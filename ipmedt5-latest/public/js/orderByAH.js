
getProducts = (product) => {
    clearModal();
 
  var requestOptions = {
method: 'GET',
redirect: 'follow'
};

fetch("https://www.ah.nl/zoeken/api/products/search?query=" + product + "&size=4", requestOptions)
.then(response => response.json())
.then(async (result) =>  {
let cards = result.cards
//   console.log(cards)
for(let i = 0;i < cards.length;i++){

  if ('shield' in cards[i].products[0]){
    cards.splice(i, 1);
  }
}
for(let i = 0;i < cards.length;i++){
      let product = cards[i].products[0]
     let productDetails = await getProductDetail(product)  

      // let details = function() => 
      // result['_embedded']['lanes']
      //     for(let i = 0; i < details.length;i++){
      //         if (details[i]['type'] == "ProductDetailLane"){
      //             console.log(details[i]['_embedded']['items'][0]['_embedded']['product'])
                  
      //     }
      //     }
      // productDetails = details[i]['_embedded']['items'][0]['_embedded']['product']
      let productItem = document.createElement('li')
      productItem.setAttribute('data-productid', product.id)
      productItem.setAttribute('onclick', 'addToCart("' + product.id + '")')
      productItem.setAttribute('class','list-group-item list-group-item-action flex-column align-items-start')
      let productElm = document.createElement('div')
      productElm.setAttribute('class', 'd-flex w-100')
      let productDet = document.createElement('div')
      productDet.setAttribute('class', 'd-flex w-100 justify-content-between')
      let productImg = document.createElement('img')
      let productTitlePlaceholder = document.createElement('div')
      let productTitle = document.createElement('h5')
      productTitlePlaceholder.appendChild(productTitle)
      let productPrice = document.createElement('span')
      productPrice.setAttribute('style', 'align-self: flex-end;')
      let productPriceText = document.createTextNode(productDetails.priceLabel.now)
      let eenheid = document.createElement('small')
      let eenheidText = document.createTextNode(productDetails.unitSize)
      let melding = document.createElement('div')
      melding.setAttribute('style', 'margin-left: 15px;color: rgb(255, 204, 2);font-style: italic;')
      let meldingText = document.createElement('small')
      meldingText.setAttribute('id', 'js--' + product.id + "Melding")
      melding.appendChild(meldingText)
      eenheid.setAttribute('style', 'margin-left: 5px;')
      eenheid.appendChild(eenheidText)
      productTitlePlaceholder.appendChild(eenheid)
      productTitlePlaceholder.appendChild(melding)
      productPrice.appendChild(productPriceText)
      productTitle.setAttribute('class', 'mb-1')
      productTitle.setAttribute('style', 'margin-left: 15px; display: inline-block;')
      let productTitleText = document.createTextNode(product.title);
      productTitle.appendChild(productTitleText)
      productImg.setAttribute('style', 'width: 100px; height: 100px;')
      productImg.setAttribute('src', product.images[0].url)
      productElm.appendChild(productImg)
      productDet.appendChild(productTitlePlaceholder)
      productDet.appendChild(productPrice)
      if(productDetails.discount){
          if(productDetails.discount.type.name == "BONUS"){
              let discount = document.createElement('span')
              let discountText =  document.createTextNode(productDetails.discount.label)
              if(productDetails.discount.label == "BONUS"){
                  productPrice.setAttribute('style', 'align-self: flex-end; color: #ff7900;')
              }
              discount.setAttribute('style', 'height: 22px; font-size: 13px; position: absolute; right: 20px; border-radius: 2px; padding: 4px; background-color: #ff7900; color: #fff; line-height: 1.15; font-weight: 900; display: block; letter-spacing: .6px;')
              discount.appendChild(discountText)
              productDet.appendChild(discount)
          }
      }
      if(!productDetails.availability.orderable){
          productItem.classList.add("disabled");
          productItem.setAttribute('style', 'opacity: 0.5')
      }
      productElm.appendChild(productDet)
      productItem.appendChild(productElm)
      let productHolder = document.getElementById('products')
      productHolder.appendChild(productItem)
}

})

.catch(error => console.log('error', error));
setTimeout(()=>{
  listCart()
}, 800)
}
clearModal = () => {
    document.getElementById('products').innerHTML = "";
}
// getProductDetail = async (product) => {
//     return await fetch("https://www.ah.nl/service/rest/delegate?url=" + product.link)
//             // .then(response => response.json())
//             // .then((result) => {
//             //     return result
//             // })
//             // .catch(error => console.log('error', error));
// }

async function getProductDetail(product) 

{
let data = await fetch("https://www.ah.nl/service/rest/delegate?url=" + product.link)
.then((response) => response.json())
.then(data => {
  data = data['_embedded']['lanes'];
  for(let i = 0; i < data.length;i++){
      if (data[i]['type'] == "ProductDetailLane"){
          return data[i]['_embedded']['items'][0]['_embedded']['product']
                  
      }
  }
})
.catch(error => {
  console.error(error);
});
return data
}

function addToCart(product){
    product = product.replace('wi','');
    var myHeaders = new Headers();
    myHeaders.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    myHeaders.append('Access-Control-Allow-Headers', 'Content-Type, X-CSRF-TOKEN');
    var requestOptions = {
        method: 'POST',
        redirect: 'follow',
        headers: myHeaders
      };
fetch("/product/addtocart/" + product, requestOptions)
  .then(response => response.json())
  .then((result) => {
    if(result.status == 500){
      document.getElementById('js--errorCardAH').style.display = "block"
      setTimeout(() =>{
        document.getElementById('js--errorCardAH').style.display = "none"
      }, 5000)
    }
    else{
      listCart();
      // clearModal();
      document.getElementById('js--AlertAhCartSuccess').style.display = "block";
      // setTimeout(function(){ 
      //   document.getElementById('js--AlertAhCartSuccess').style.display = "none";
        

      // }, 3000);
    }

  })
  .catch(error => console.log('error', error.status));
  pingCart()
}
function pingCart(){
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };


fetch("/product/getCart", requestOptions)
  .then((response) => response.json())
  .then(data =>{
    if(data.status == 500){
      document.getElementById('js--errorCardAH').style.display = "block"
      setTimeout(() =>{
        document.getElementById('js--errorCardAH').style.display = "none"
      }, 5000)
    }
    if (data.summary.quantity > 0){
      document.getElementById('js--toastAHTitle').innerText = data.summary.quantity + " boodschappen in winkelwagentje"
      $('.toast').toast('show');
    }
  })
  .catch(error => console.log('error', error));
}
setInterval(() => {
  pingCart()
}, 60000)


function listCart(){
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };


fetch("/product/getCart", requestOptions)
  .then((response) => response.json())
  .then(data =>{
    let products = document.getElementById('products').childNodes;
    // console.log(products);
    data = data.products
    products.forEach((product) =>{
      console.log(product)
      let productId = product.getAttribute('data-productid')
      console.log(data)
      data.forEach((cartProduct) => {
          console.log(cartProduct)
          console.log(cartProduct.id)
          if (cartProduct.id == productId){
            document.getElementById('js--' + product.getAttribute('data-productid') + 'Melding').innerText = 'Dit product zit al in uw AH winkelwagentje' + " (" + cartProduct.quantity + "x)"
          }
      })



    })

  })
  .catch(error => console.log('error', error));

}
