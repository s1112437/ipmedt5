// window.addEventListener("load", function(){
//     // [1] GET ALL THE HTML ELEMENTS
//     var video = document.getElementById("vid-show"),
//         canvas = document.getElementById("vid-canvas"),
//         take = document.getElementById("vid-take");
  
//     // [2] ASK FOR USER PERMISSION TO ACCESS CAMERA
//     // WILL FAIL IF NO CAMERA IS ATTACHED TO COMPUTER
//     navigator.mediaDevices.getUserMedia({ video : true })
//     .then(function(stream) {
//       // [3] SHOW VIDEO STREAM ON VIDEO TAG
//       video.srcObject = stream;
//       video.play();
  
//       // [4] WHEN WE CLICK ON "TAKE PHOTO" BUTTON
//       take.addEventListener("click", function(){
//         // Create snapshot from video
//         var draw = document.createElement("canvas");
//         draw.width = video.videoWidth;
//         draw.height = video.videoHeight;
//         var context2D = draw.getContext("2d");
//         context2D.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
//         // Output as file
//         var anchor = document.createElement("a");
//         anchor.href = draw.toDataURL("image/png");
//         anchor.download = "webcam.png";
//         anchor.innerHTML = "Click to download";
//         canvas.innerHTML = "";
//         canvas.appendChild(anchor);
//       });
//     })
//     .catch(function(err) {
//       document.getElementById("vid-controls").innerHTML = "Please enable access and attach a camera";
//     });
//   });

 var makePictureButton = document.getElementById("js--take")
    var countDown = 5;
 makePictureButton.addEventListener('click', function(){
  makePictureButton.style.display = 'none';
    var video = document.getElementById("vid-show"),
    canvas = document.getElementById("vid-canvas"),
    overlayText = document.getElementById('js--videoOverlayText')
    
    navigator.mediaDevices.getUserMedia({ video : true })
    .then(function(stream) {
      // [3] SHOW VIDEO STREAM ON VIDEO TAG
      video.srcObject = stream;
      video.play();
      document.getElementById('js--videoOverlay').style.display = 'flex';
      setInterval(function(){
          if(countDown >= 0){
            overlayText.innerText = countDown--
          }
      }, 1000);
      setTimeout(function(){
        video.pause()
        var draw = document.createElement("canvas");
        draw.width = video.videoWidth;
        draw.height = video.videoHeight;
        var context2D = draw.getContext("2d");
        context2D.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        imgAsText = draw.toDataURL("image/png")
        document.getElementById('js--picture').value = imgAsText
        uploadFile(imgAsText)
      }, (countDown * 1000 + 1000))
    })
    .catch(function(err) {
        document.getElementById("vid-controls").innerHTML = "Please enable access and attach a camera";
    })
 })