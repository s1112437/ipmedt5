echo "SYSTEM - Start PHP flush"
php artisan down
composer install
php artisan cache:clear
php artisan config:cache
php artisan route:cache
php artisan migrate
php artisan up
echo "SYSTEM - Deploy finished"