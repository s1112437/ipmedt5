<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('products')->insert([
              'name' =>  'Broccoli',
              'user_id' => 1,
              'expiration_date' => '1587135980',
              'category' => 'plantaardige producten',
              'image' => '1585130875.png',
              'created_at' => '2020-03-18 16:25:16',
              'updated_at' => '2020-03-18 16:25:16'
            ]);
            DB::table('products')->insert([
                'name' =>  'Broccoli',
                'user_id' => 1,
                'expiration_date' => '1587135980',
                'category' => 'plantaardige producten',
                'image' => '1585130875.png',
                'created_at' => '2020-03-18 16:25:16',
                'updated_at' => '2020-03-18 16:25:16'
            ]);
            DB::table('products')->insert([
              'name' =>  'Appel',
              'user_id' => 1,
              'expiration_date' => '1592147180',
              'category' => 'fruit',
              'image' => '1585130875.png',
              'created_at' => '2020-03-18 16:25:16',
              'updated_at' => '2020-03-18 16:25:16'
          ]);
        }
      
    }
}