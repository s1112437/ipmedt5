# Slimme koelkast 🥶

Project groep bestaande uit:

- Klaas
- Aaron
- Anne Sophie
- Lars
- Julian

# ℹ️ Gebruik van deze repository

Om zoveel mogelijk git probleempjes te voorkomen, zijn er een aantal richtlijnen:

- De `dev` branch is de bakermat van de development.
- 🤷 Wanneer je aan de slag gaat (nieuwe features/bugfix etc.) werk je in een branch die afstamt van de `dev` branch. Middels een **Pull Request** met als target branch `dev`, voeg je uiteindelijk je wijzigingen samen.
- 📅 Wanneer we tot een stable release komen, volgt er een PR van `dev` naar `master` met een release tag.
- ⛔ Er mogen geen directe commits worden uitgevoerd op de `dev` en `master` branch.
- 🔎 Een PR dient door iemand anders dan je zelf te worden **gereviewed** en **gemerged**.

## 🔧 Pipelines

Bij een merge / commit op de `dev` branch, zal middels een pipeline een versie kopie van de repo op een VPS geplaats worden. Plus nog wat scripts uitvoeren om alles goed te laten draaien.

Bekijk de "_the moste stable version_" van ons koelkast project op: [www.ipmedt5.live](http://ipmedt5.live)


# 🧰 Hoe aan de slag?

Hieronder staat beschreven hoe je een werk omgeving kan opzetten. Kom je er niet uit? Geef dit dan aan in de whatsapp groep 😄 Bekijk ook vooral de gemaakte video van Lars [install-uitleg.mp4](https://bitbucket.org/s1112437/ipmedt5/src/dev/install-uitleg.mp4).

## Voorbereiding

Voordat je een werk omgeving opzet, is het eerst belangrijk om de MySQL database in orde te krijgen. Database draaien in docker? Ask klaas.

1. Login in je MySQL omgeving.
2. Maak een nieuwe database aan: `CREATE DATABASE [NAAAM DATABASE];`
3. (optioneel) Maak een nieuwe gebruiker aan: `CREATE USER '[GEBRUIKERSNAAM]'@'localhost' IDENTIFIED BY '[WACHTWOORD]';`
4. Geef de (nieuwe) gebruiker rechten over de gemaakte database: `GRANT ALL PRIVILEGES ON [DATABASE NAAM].* TO '[GEBRUIKERSNAAM]'@'localhost';`

## Opzetten werk omgeving

1. Clone deze repository naar een gewenste directory: `git clone git@bitbucket.org:s1112437/ipmedt5.git`
2. Zorg er voor dat je in MySQL [een database hebt met username en (native) password](#voorbereiding) 
3. Navigeer in je code editor naar de `ipmedt5/ipmedt5-latest` directory
4. Maak een kopie van de file: `.env.example` en hernoem het naar `.env`
5. Pas de database gegevens aan in de `.env` file met jouw eigen MySQL gegevens
6. Open een terminal in de `ipmedt5-latest` directory
7. Voer uit: `composer install`
8. Voer uit: `php artisan key:generate`
9. Check in de `.env` file of de variable `API_KEY=` is ingevuld
10. Voer uit: `php artisan migrate`
11. Voer uit: `php artisan serve`

Als het goed is moet je nu via de URL een scherm krijgen waar je kan inloggen.

# ➕ Handige VSCODE extensions

- [Laravel Blade Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-blade)
- [Laravel Artisan](https://marketplace.visualstudio.com/items?itemName=ryannaddy.laravel-artisan)
- [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack)

# 🆘 Eerste hulp bij GIT problemen

Problemen met GIT? Onderstaand een aantal handige oplossingen om snel weer aan de slag te kunnen.

## Je werk middels een PR (Pull Request) mergen in de `dev` branch

Wanneer je klaar bent met je werk en deze graag wil samenvoegen in het project, voer je het volgende uit:

1. Zorg er voor dat al je werk middels `commits`, gepusht is naar de remote (Bitbucket)
2. Ga vervolgens naar de [lijst met Pull Requests](https://bitbucket.org/s1112437/ipmedt5/pull-requests/) op Bitbucket
3. Klik op **Create pull request**
4. Controleer of je de juiste branch aanbied om te mergen
5. Controleer onderaan bij **Diffs** of er mogelijk een **merge conflict** optreed. Dit herken je eenvoudig middels een grote gele waarschuwing. In het geval van een merge conflict, dien je nog een [rebase](#bij-blijven-met-de-dev-branch) uit te voeren.
6. Geef de PR een titel en beschrijving
7. Klik op **Create pull request**

## Bij blijven met de `dev` branch

Als je je eigen branch wil bijwerken met de laatste aanpassingen van de `dev` branch, dien je een **rebase** uit te voeren. Dit kan eenvoudig door onderstaande stappen te volgen:

*In geval van openstaande wijzigingen, krijg je een foutmelding. Daarom eerst al je werk committen voordat je een rebase uitvoert. Of probeer [onderstaande methode](#tussen-door-rebasen-met-uncommitted-werk)* ⬇️

```bash
git checkout [BRANCH NAAM]
git fetch origin dev
git rebase origin/dev
```

Als er met een rebase conflicten ontstaan, herhaal dan onderstaand proces totdat alle wijzigingen zijn opgelost:

1. `git status` toont je het bestand met het conflict; bewerk het bestand en los de lijnen op tussen `<<<< | >>>>`
2. Voeg het gewijzigde bestand toe: `git add <file>` of `git add.`
3. Doorgaan met de rebase: `git rebase --continue`
4. Herhaal dit totdat je alle conflicten hebt opgelost

Raak je in paniek? Breek dan de rebase af met: `git rebase --abort`.

### Tussen door rebasen met uncommitted werk

Het is mogelijk om tussentijds te rebasen, terwijl je nog changes open hebt staan. Hiervoor maak je gebruik van **stash**:

1. Zet je huidige uncommitted changes weg middels `git stash save [MESSAGE]`
2. Voer een [rebase](#bij-blijven-met-de-dev-branch) uit zoals hierboven
3. Zet je stash terug middels `git stash pop` (`pop` pakt automatich stash{0} en verwijderd deze ook meteen)

Bekijk je lijst met stashes middels:

```bash
git stash list
```

Verwijder een specifieke stash: (het nummer tussen {} verwijst naar het nummer in de stash list)

```bash
git stash drop stash@{1}
```

Verwijder alles in een keer:

```bash
git stash clear
```

## Hoe maak je een nieuwe branch

Als je een nieuwe branch wil maken die afstamt van de `dev` branch, voer je het volgende uit:

```
git checkout dev
git pull
git checkout -b [NIEUWE BRANCH NAAM]
```

Kort samengevat zorgt bovenstaande er voor dat je zeker op de `dev` branch zit, je de laatste wijzigingen binnen hebt gehaald en de laatste stap maakt daadwerkelijk een nieuwe branch voor je aan.

## Hoe verwijder je een branch local en op de remote?

Op je lokale machine:

```
git branch -d [BRANCH NAAM]
```

Op de remote:

```
git push origin -d [BRANCH NAAM]
```

---
Dit project is naar aanleiding van een studie project aan de Hogeschool van Leiden.