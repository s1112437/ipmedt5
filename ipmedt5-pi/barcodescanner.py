import requests
import json

import base64
import hashlib
import hmac


def make_auth_token(upc_string, auth_key):
    sha_hash = hmac.new(str.encode(auth_key), str.encode(upc_string), hashlib.sha1)
    return base64.b64encode(sha_hash.digest())


upc = "8852018101024"
app_key = "/3PBWawIw9Nn"
auth_key = "Ad26Z0d8w5Gd1Hx0"
signature = make_auth_token(upc, auth_key)  # TAbjYip3By3NRV9XyxHB9DauAdM=
print(signature)
'''V3 API'''
print(upc)

url = "https://www.digit-eyes.com/gtin/v2_0/?upcCode=%s&language=en&app_key=%s&signature=%s"

headers = {
    'cache-control': "no-cache",
}

print(headers)

request_url = url % (upc, app_key, signature.decode())
print(request_url)
response = requests.get(request_url, headers=headers)

print("-----" * 5)
print(upc)
print(json.dumps(response.json(), indent=2))
print("-----" * 5 + "\n")
