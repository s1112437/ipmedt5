#!/usr/bin/env python3
import serial
from flask import Flask
import json
import time

app = Flask(__name__)


@app.route('/temperature')
def hello_world():
    stats = {
        "temperature": 0,
        "humidity": 0,
        "heat_index": 0
    }
    ser.write('1\n'.encode())

    time.sleep(1)

    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
        stats = json.loads(line)
        ser.flush()
    return stats


if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
    ser.flush()
    app.run(host='192.168.1.246', port=80, debug=True)
